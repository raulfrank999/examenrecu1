/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenrecu;

/**
 *
 * @author LEGION 5 15IMH05H
 */
public class VehiculoCarga extends Vehiculo{
    private float toneladasCarga;

    public VehiculoCarga(float toneladasCarga, int serie, int tipoMotor, String marca, String modelo) {
        super(serie, tipoMotor, marca, modelo);
        this.toneladasCarga = toneladasCarga;
    }
    public VehiculoCarga(){
        
        this.toneladasCarga = 0.0f; 
    
    }

    public float getToneladasCarga() {
        return toneladasCarga;
    }

    public void setToneladasCarga(float toneladasCarga) {
        this.toneladasCarga = toneladasCarga;
    }
    
    
}
