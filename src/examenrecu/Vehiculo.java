/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenrecu;

/**
 *
 * @author LEGION 5 15IMH05H
 */
public  abstract class Vehiculo{
    protected int serie;
    protected int tipoMotor;
    private String marca;
    protected String modelo;

    public Vehiculo(int serie, int tipoMotor, String marca, String modelo) {
        this.serie = serie;
        this.tipoMotor = tipoMotor;
        this.marca = marca;
        this.modelo = modelo;
    }
    public Vehiculo(){
        this.serie = 0;
        this.tipoMotor = 0;
        this.marca = "";
        this.modelo = "";
        
        
    }

    public int getSerie() {
        return serie;
    }

    public void setSerie(int serie) {
        this.serie = serie;
    }

    public int getTipoMotor() {
        return tipoMotor;
    }

    public void setTipoMotor(int tipoMotor) {
        this.tipoMotor = tipoMotor;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }
}
