/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenrecu;

/**
 *
 * @author LEGION 5 15IMH05H
 */
public class Registro {
    private int numRegistro;
    private String fecha;
    private int tipo;
    private VehiculoCarga vehiculoCarga;
    private String descripcion;
    private float costoBase;

    public Registro(int numRegistro, String fecha, int tipo, VehiculoCarga vehiculoCarga, String descripcion, float costoBase) {
        this.numRegistro = numRegistro;
        this.fecha = fecha;
        this.tipo = tipo;
        this.vehiculoCarga = vehiculoCarga;
        this.descripcion = descripcion;
        this.costoBase = costoBase;
    }
    public Registro(){
        this.numRegistro = 0;
        this.fecha = "";
        this.tipo = 0;
        this.vehiculoCarga = new VehiculoCarga();
        this.descripcion = descripcion;
        this.costoBase = costoBase;
    }

    public int getNumRegistro() {
        return numRegistro;
    }

    public void setNumRegistro(int numRegistro) {
        this.numRegistro = numRegistro;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public VehiculoCarga getVehiculoCarga() {
        return vehiculoCarga;
    }

    public void setVehiculoCarga(VehiculoCarga vehiculoCarga) {
        this.vehiculoCarga = vehiculoCarga;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public float getCostoBase() {
        return costoBase;
    }

    public void setCostoBase(float costoBase) {
        this.costoBase = costoBase;
    }
    public float CalcularTotal(int x){
        float total = 0;
        if(x ==0){total = this.costoBase*1.25f;}
        if(x==1){total = this.costoBase *1.50f;}
        if(x==2){total = this.costoBase *2.50f;}
        if(x==3){total = this.costoBase *3.00f;}
    return total;
    }
    public float CalcularImpuesto(int x){
        float impuesto = 0.0f;
        impuesto = this.CalcularTotal(x)*0.16f;
    return impuesto;
    }
}
